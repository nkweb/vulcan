<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *	 least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *	 or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *	 when linking to the front page. This includes the language domain or
 *	 prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *	 in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *	 in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *	 site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *	 the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *	 modules, intended to be displayed in front of the main title tag that
 *	 appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *	 modules, intended to be displayed after the main title tag that appears in
 *	 the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *	 prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *	 (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *	 menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *	 associated with the page, and the node ID is the second argument
 *	 in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *	 comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content_top']: Items for the header region.
 * - $page['content']: The main content of the current page.
 * - $page['content_bottom']: Items for the header region.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>

<div id="page-wrapper">
	<div id="page">
		<header id="header" role="banner" class="clearfix">
			<div class="container-12 header-top-wrapper">
				<div class="header-top">
					<div class="clearfix">
						<div class="grid-3 alpha">
							<?php if ($logo): ?>
								<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
									<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
								</a>
							<?php endif;

							if ($site_name || $site_slogan): ?>
								<div id="name-and-slogan">
									<?php if ($site_name):
										if ($title): ?>
											<div id="site-name">
												<strong><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a></strong>
											</div><!-- /#site-name -->
										<?php else: /* Use h1 when the content title is empty */ ?>
											<h2 id="site-name">
												<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
											</h2>
										<?php endif;
									endif;

									if ($site_slogan): ?>
										<div id="site-slogan"><?php print $site_slogan; ?></div>
									<?php endif; ?>
								</div><!-- /#name-and-slogan -->
							<?php endif; ?>
						</div>

						<div class="grid-9 omega">
							<?php if ($page['menu']):
								print render($page['menu']);
							endif; ?>
							<?php if ($page['search']):
								print render($page['search']);
							endif; ?>
						</div>
					</div>
				</div>
			</div>

			<?php if ($page['header']):?>
				<div class="header-region-wrapper">
					<?php print render($page['header']);?>
				</div>
			<?php endif;?>

			<?php if ($page['header_bottom']):?>
				<div class="header-bottom-region-wrapper">
					<div class="container-12">
						<div class="grid-12">
							<?php print render($page['header_bottom']); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</header><!-- /#header -->
		<div id="main-wrapper">
			<?php if ($page['content_top']): ?>
				<div class="content-top-wrapper">
					<div class="container-12">
						<div class="grid-12 clearfix">
							<?php print render($page['content_top']); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php if ($page['content_top2']):?>
				<div class="content-top2-wrapper">
					<div class="container-12">
						<div class="grid-12 clearfix">
							<?php print render($page['content_top2']); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="container-12">
				<div class="grid-12">
					<div id="main" class="clearfix">
						<?php if ($page['sidebar_first']): ?>
							<aside id="sidebar-first" class="column column_left sidebar grid-3 suffix-1 alpha" role="complementary">
								<div class="section">
									<?php print render($page['sidebar_first']); ?>
								</div>
							</aside>
						<?php endif; ?>

						<div id="content" class="column" role="main">
							<div class="section">
								<?php if ($breadcrumb): ?>
									<div id="breadcrumb" class="clearfix"><?php print $breadcrumb; ?></div>
								<?php endif;

								if ($messages): ?>
									<div id="messages">
										<div class="section clearfix">
											<?php print $messages; ?>
										</div>
									</div> <!-- /.section, /#messages -->
								<?php endif;

								print render($title_prefix);
									if ($title): ?>
										<h1 class="title" id="page-title"><?php print $title; ?></h1>
									<?php endif;
								print render($title_suffix);

								if ($tabs): ?>
									<div class="tabs">
										<?php print render($tabs); ?>
									</div>
								<?php endif;

								print render($page['help']);

								if ($action_links): ?>
									<ul class="action-links"><?php print render($action_links); ?></ul>
								<?php endif;

								print render($page['content']); ?>
							</div>
						</div>

						<?php if ($page['sidebar_second']): ?>
							<aside id="sidebar-second-" class="column column_right sidebar grid-3 prefix-1 omega" role="complementary">
								<div class="section">
									<?php print render($page['sidebar_second']); ?>
								</div>
							</aside>
						<?php endif; ?>
					</div><!-- /#main -->

					<?php if ($page['content_bottom']):
						print render($page['content_bottom']);
					endif; ?>
				</div>
			</div>
		</div>
		<footer id="footer" role="contentinfo">
			<div class="container-12">
				<div class="grid-12">
					<div class="footer-wrapper clearfix">
						<?php print render($page['footer']);
					?>
					</div>
				</div>
			</div>
		</footer>
	</div><!-- /#page -->
</div><!-- /#page-wrapper -->
